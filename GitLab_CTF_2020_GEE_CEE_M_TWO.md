# GitLab CTF@Contribute 2020: GEE CEE M TWO

Category: `crypto`  
Points: 500  
Description:

---

Can you break the cipher and [encode](https://geeceem.thetanuki.io/redeem/goodluck) the token? (Yes HTTP 500 is intentional on that link)

---

Long description:

---

Can you break the cipher and [encode](https://geeceem.thetanuki.io/redeem/goodluck) the following token?

```
class TokenController < ApplicationController
  include CryptoHelper
  def redeem
     decrypted = aes256_gcm_decrypt(params[:token])
     if decrypted == "caughtthetanuki"
       render plain: "FLAG:D"
     else
       render plain: "noot"
     end

  end
end
```
The naive solution is not allowed:

```
class User < ApplicationRecord
  before_validation(on: :create) do
    if attribute_present?("token")
      self.token = "noot" if token.match("tanuki")
    end
  end
before_save      EncryptionWrapper.new
end
```
The route to the TokenController is:

`get 'redeem/:token', to: 'token#redeem'` so bear in mind the URL encoding!

---

### Intro

This challenge builds on top of `GEE CEE M ONE`, and this writeup will also refer to some parts of that writeup. Specifically around terminology and general setup of the challenge.

The goal of this challenge is to defeat the built-in tamper-protection of GCM. GCM applies authentication to the plaintext, so that even if the keystream is known, the ciphertext cannot be altered without failing decryption. With one exception. If the nonce/IV was reused for multiple messages, this can be leveraged to compute the auth-tag, without necessarily knowing what the key was.

### Recon

In the code we can see what we need to attack. If the provided token can be decrypted to `caughtthetanuki` we will be presented with the flag. But trying to create a token with the word `tanuki` in it, will set it's content to `noot`. So we cannot naively just create one.

Because we are an efficient attacker, we want to exploit the path of least resistence. So the first thing I did, was to check whether the auth-tag was even checked properly. In order to do this, we encrypt a set of new tokens, change the auth-tag and afterwards validate whether the app wants to decrypt it.

We have a few possible scenarios there, that might be implementation bugs

1. Auth tag length is verified, but it itself is not checked.

To test this theory, we are going to encrypt a token with the content of `cbubhbtbebbbubi`.

The resulting Token will be `ENdA+CvXyy/evZ+m3TJTGvScsOr2SUSoohJLs5PhIQ==`. In order to check whether the auth-tag is validated properly, we are going to decode and decrypt the token, and alter the first 15 bytes to be `caughtthetanuki`. Afterwards, reencrypt and reencode, so we will get `ENRA/SvByyXeq5yq3TtTGvScsOr2SUSoohJLs5PhIQ==`.

This token we will url-encode and append to the url from the description. Without success. The auth-tag is indeed properly validated.

2. Auth-tag is properly validated and we need to find another attack.

Since the tag is validated, we are going to use the `Forbidden Attack` described in chapter 3 of [Nonce-Disrespecting Adversaries:
Practical Forgery Attacks on GCM in TLS](https://www.nds.ruhr-uni-bochum.de/media/nds/veroeffentlichungen/2016/08/15/nonce-disrespect-woot.pdf).

The tl;dr; of this attack is that because of the reuse of the nonce (the IV and counter) we can factor the possible roots of the polynomial used for signing to retrieve candidate authentication keys. Those are then used to sign the provided plaintext.

While we are not attacking TLS, but rather static data, the general principle still applies, with some modifications.  

We will not have associated data (*A*) and thus, forming the block encoding of *A* and *C*, *L* is also not required for computation.  
Another modification, is that in our case we will operate on plaintexts, instead of ciphertexts.  
As stated in `GEE CEE M ONE` is possible, because *C<sub>i</sub> = Enc<sub>k</sub>(J<sub>i</sub>) ⊕ P<sub>i</sub>*, where *C<sub>i</sub>* is the <i>i</i>th ciphertext, *Enc<sub>k</sub>(J<sub>i</sub>)* representing the <i>i</i>th block of the keystream with key <i>k</i>, *J<sub>i</sub>* being the IV/counter and *P<sub>i</sub>* being the <i>i</i>th plaintext.  
*Enc<sub>k</sub>(J<sub>i</sub>)* is the same for all ciphertexts of the <i>i</i>th block, because of the reuse of *J<sub>i</sub>*. Thus we can reduce the complexity of our equation a bit further by using the plaintext in our equations instead.

The auth-tag *T* is equivalent to *S ⊕ Enc<sub>k</sub>(J<sub>0</sub>)*. Because we are solving for the plaintext, we are also not solving for *T*, but for *S* instead.

*g(H) = T*

*H* is a root of *g<sub>1+2</sub>*, thus, because we [XORed the plaintexts](https://crypto.stackexchange.com/q/59156) (*P<sub>1</sub> ⊕ P<sub>2</sub> = P<sub>m</sub>*) and auth-tags (*S<sub>1</sub> ⊕ S<sub>2</sub> = S<sub>m</sub>*) we can say *H* is a root of *g<sub>m</sub>* (*m* meaning merged).

*g<sub>m</sub>(X) = P<sub>m</sub> * X + S<sub>m</sub>*

is thus equivalent to

*g<sub>1+2</sub>(X) = (P<sub>1</sub> + P<sub>2</sub>) * X + (S<sub>1</sub> + S<sub>2</sub>)*

In the end *g<sub>m</sub>(H) = 0* which allows us to factor the polynomial *X* and determine candidates values for *S<sub>3</sub>* (the plaintext auth-tag).

We compute the roots for *g<sub>m</sub>* and for each root (*H*) we then plug it into

*f(H) = P<sub>1</sub> * X + S<sub>1</sub> + P<sub>3</sub> * X*

where *P<sub>3</sub>* is the plaintext for which we need the auth-tag.
This returns us a potential *S<sub>3</sub>*, the auth-tag we need.

For full details, you can refer to the [exploit code](./GEE_CEE_M_TWO.py)

### Exploit

Firstly we need to generate 2 random strings of 15 bytes (the same length as `caughtthetanuki`).

```
$ pwgen 15 2
Aev8weyeeRolie0 eegh9ieVae4Ka4u
```

Those will be encrypted the same way we did in `GEE CEE M ONE` via the website.

```
MtBDojTQxijejZKowTUKl5ApVqFDOsHc3xoCsnZTXg==
FtBS8nrc2hvausmPyWRPpLddPjmcdbcvT6oegjI+yw==
```

Now we need to decode and decrypt the tokens to mount our attack. We will be reusing the same `keystream.bin` as in `GEE CEE M ONE` as our message is smaller than 64 bytes.

```
$ echo -n "MtBDojTQxijejZKowTUKl5ApVqFDOsHc3xoCsnZTXg==" | base64 -d | python ~/xcat.py -f keystream.bin > plaintext1.bin
$ echo -n "FtBS8nrc2hvausmPyWRPpLddPjmcdbcvT6oegjI+yw==" | base64 -d | python ~/xcat.py -f keystream.bin > plaintext2.bin
```

Lastly create our target :)

```
$ echo -n caughtthetanuki > target.bin
```

And run the exploit

```
$ docker run -w `pwd` -v `pwd`:`pwd`:rw sagemath/sagemath:8.9 ./GEE_CEE_M_TWO.py plaintext1.bin plaintext2.bin target.bin
Plaintext 1: 416576387765796565526f6c69653000
Auth-tag 1: f0e10adef76515fc25f4acf525d9e42e
Plaintext 2: 65656768396965566165344b61347500
Auth-tag 2: c3c67eb66fba5a8ad6641ce9159d89bb
Forgery target: 63617567687474686574616e756b6900
Candidate Tag 1 e9e3c3151479d2fabfcf1ddd9931a006
```

Success! We have a candidate auth-tag!  
The exploit will have created a `target.bin_candidate_1.bin`, which is ready to be XORed with the keystream. So lets do that. And while we're at it, also encode it :)

```
$ python ~/xcat.py -f keystream.bin target.bin_candidate_1.bin | base64
ENRA/SvByyXeq5yq3TtTjpLgnUJf/cdG5KsqDp4Xdg==
```

In the last step we just need to claim our reward, the flag. Url-encoding the token and appending it to the url from the description yields this wonderful string:

```
[tanuki](caefc7db14e95a4a895322fdb15e2f5a)
```

Wow! That was worth the effort :)
