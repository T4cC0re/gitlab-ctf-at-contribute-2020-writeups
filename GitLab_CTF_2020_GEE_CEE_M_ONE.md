# GitLab CTF@Contribute 2020: GEE CEE M ONE

Category: `crypto`  
Points: 200  
Description:

---

Can you break the cipher and decode [the admin's](https://geeceem.thetanuki.io/users) token?

Make sure to have a look at the long description

Note: created users will be deleted after ten minutes.

---

Long description:

---

Can you break the cipher and decode [the admin's](https://geeceem.thetanuki.io/users) token?

```
module CryptoHelper
  extend self

  AES256_GCM_OPTIONS = {
    algorithm: 'aes-256-gcm',
    key: Rails.application.credentials.aes_key,
    iv: Rails.application.credentials.aes_iv
  }.freeze

  def aes256_gcm_encrypt(value)
    encrypted_token = Encryptor.encrypt(AES256_GCM_OPTIONS.merge(value: value))
    Base64.strict_encode64(encrypted_token)
  end

  def aes256_gcm_decrypt(value)
    return unless value

    encrypted_token = Base64.decode64(value)
    Encryptor.decrypt(AES256_GCM_OPTIONS.merge(value: encrypted_token))
  end

end
```
```
class EncryptionWrapper
include CryptoHelper
  def before_save(record)
    record.token = aes256_gcm_encrypt(record.token)
  end

  def after_save(record)
    record.token = aes256_gcm_decrypt(record.token)
  end

  alias_method :after_initialize, :after_save
end
```
```
class User < ApplicationRecord
before_save      EncryptionWrapper.new
end
```

---

### Intro

The linked page is a simple HTML page exposing information about a user, a token attached with a message about that user.  
It also nicely points out a record, which contains the encrypted token.

Roughly like this:

Name | Token | Message
--- | --- | ---
`admin` | `KMFU9Dbe1hCT78SlnTEJV0IQuGVETFzIEoDOrpfURtH4WNg+r009lJSBth3NhorCMxgW/nkw90WKuw==` | `<= the flag is right there`

Another thing it does, is to provide us with means to encrypt any message we like, by creating a `New User`. On that page we can enter an arbitrary `Name`, `Token` and `Message`. Name and Message are just strings without function, but `Token` will be encrypted with the configured AES settings and base64 encoded. This will then be visible on the main page to extract the encrypted message (=`Token`).

```
def aes256_gcm_encrypt(value)
  encrypted_token = Encryptor.encrypt(AES256_GCM_OPTIONS.merge(value: value))
  Base64.strict_encode64(encrypted_token)
end
```

Before we go into how to break this challenge, let me start with a little primer on AES.

AES is an encryption standard, which can be used in a variety of modes.  
We are going to limit ourselves to **CTR** (Counter) and **GCM** (Galois/Counter Mode) for now, which are stream ciphers, meaning they can operate on an arbitrary size of data.  
AES CTR works, by having a monotonously increasing counter initialized by the IV (initialization vector) to feed into AES in order to create a block (128 bits) worth of data. This can be repeated as many times as required, depending on the length of the input. This forms the so called `keystream`. The input (=`plaintext`) is XORed with the keystream to create the `ciphertext`.

CTR and GCM work fundamentally identical, with the small difference that GCM adds authentication to decryption. This is done by adding an auth-tag to the end of the ciphertext, which is then checked upon decryption. Without having a valid auth-tag decryption of a ciphertext in GCM mode will fail, thus this is used as protection against forgery.

AES GCM is generally considered secure, but only as long as you use a __*different key and IV*__ combination for __*each message*__.  
This is generally implemented, by sharing a key among messages, but generating a unique IV for each message. This IV is then usually prefixed to the ciphertext to transport it. The IV itself, in contrast to the key, is not considered confidential and can thus be public.

### Recon

As someone who has worked with AES GCM and AES CTR in the past, it did not take long for me to identify the attack vector in the source code.

Having a vulnerability in IV reuse would not allow us to compute the key or IV used, but it allows us to extract the keystream by XOR-ing a known plaintext with a known ciphertext. We can then use this keystream to decrypt any message, that is:
1. using the same key / IV combination
2. less than or equal in length of our plaintext

This is possible, because *C<sub>i</sub> = Enc<sub>k</sub>(J<sub>i</sub>) ⊕ P<sub>i</sub>*, where *C<sub>i</sub>* is the <i>i</i>th ciphertext, *Enc<sub>k</sub>(J<sub>i</sub>)* representing the <i>i</i>th block of the keystream with key <i>k</i>, *J<sub>i</sub>* being the IV/counter and *P<sub>i</sub>* being the <i>i</i>th plaintext. *Enc<sub>k</sub>(J<sub>i</sub>)* is the same for all ciphertexts of the <i>i</i>th block, because of the reuse of *J<sub>i</sub>*. Thus we can simply cancel it out.

So the first thing we want to do, is assess what we need in order to make that happen.  
For number 1, that is simple, because the code gives us the answer to this. While the key and IV might be configured, they are not dynamically generated. So each message will be encrypted with the same combination.
```
AES256_GCM_OPTIONS = {
  algorithm: 'aes-256-gcm',
  key: Rails.application.credentials.aes_key,
  iv: Rails.application.credentials.aes_iv
}.freeze
```
As we can see, the major principle, of not re-using key/IV combinations is broken. The code, combined with the possibilty to encrypt arbitrary input from us, is thus susceptible to a [Chosen-Plaintext Attack](https://simple.wikipedia.org/wiki/Chosen-plaintext_attack).

For number 2, we need to do some simple math.  
We know our flag will be in the format `[tanuki](<16 random bytes in hex>)`, so it will take up 42 bytes.
As AES GCM will add an additional 16 byte auth-tag, which is appended to the message, totaling to 58 bytes.

### Exploit

To be on the safe side we generate a sequence of 64 bytes (randomness does not matter) to use as our plaintext.
```
$ pwgen 64 1
eisoochaghohSh2AhS3ubo4ijaer2choojoh2viex1eiyoo2Sohdahcohlohmoo1
```

Posting this to the website as a new user will encrypt this to `FtxG9SzW1yzct5Ks+zgIJhlwuyNEQAmQQdeS5Z3UGIquUdJuql5tk46ZKTt81axdT98H3QYdlhciope/vFu1rk+WavXjwHawqiA8eStUbOw=`.

To get the keystream we need some sort of method to XOR 2 files. I like [`xcat.py`](https://github.com/mstrand/xcat/blob/master/xcat.py), but anything will do.

To make things easier, let's prepare some files with the data we need.
```
# Plaintext we generated above
$ echo -n "eisoochaghohSh2AhS3ubo4ijaer2choojoh2viex1eiyoo2Sohdahcohlohmoo1" > plaintext.txt
# Base64 encoded ciphertext from the website
$ echo -n "FtxG9SzW1yzct5Ks+zgIJhlwuyNEQAmQQdeS5Z3UGIquUdJuql5tk46ZKTt81axdT98H3QYdlhciope/vFu1rk+WavXjwHawqiA8eStUbOw=" | base64 -d > ciphertext.bin
```

To extract the keystream we run:
```
$ python ~/xcat.py -f ciphertext.bin plaintext.txt > keystream.bin
```

Now we have a `keystream.bin`, which we can use to decrypt up to 64 bytes of any message encrypted with the same key and IV.

In order to get our flag, all we need is to arrange a small pipe of commands and it will be printed to our terminal.
```
$ echo -n "KMFU9Dbe1hCT78SlnTEJV0IQuGVETFzIEoDOrpfURtH4WNg+r009lJSBth3NhorCMxgW/nkw90WKuw==" | base64 -d | python ~/xcat.py -f keystream.bin | cut -c -42
[tanuki](09a5a303303bca196998c649ce87e9bb)
```

Hooray! We got our flag! But what exactly did we do just now?

Firstly we decoded the base64 encoded ciphertext to get a binary representation of it. Then we XOR that binary ciphertext with the keystream, which presents us with the plaintext plus the auth-tag. But since the auth-tag is binary and we know the length of our flag is 42 bytes, we use `cut` to discard everything afterwards leaving us with just the flag.
