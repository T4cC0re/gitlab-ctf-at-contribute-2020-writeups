#!/usr/bin/env sage
# Challenge:
### GitLab CTF@Contribute 2020: GEE CEE M TWO
### Category: `crypto`
### Points: 500
# Exploit
### Author: Hendrik 'T4cC0re' Meyer
# Notes:
### Run with Sage < 9. This is python2 code.
### Tested input is 1 block, but in theory this should work on multi-block payloads, too if adjusted a bit.
### All plaintexts are expected to have the same length

from sage.all import sys, GF, PolynomialRing

### Functions
def read_input(input_file):
    with open(input_file, 'r') as fd:
        content = fd.read()
        auth_tag_pos = len(content) - 16
        auth_tag = content[auth_tag_pos:]
        plaintext = content[0:auth_tag_pos]
        return auth_tag_pos, pad_to_block(plaintext), pad_to_block(auth_tag)

def read_forgery(input_file):
    with open(input_file, 'r') as fd:
        content = fd.read()
        return len(content), pad_to_block(content)

def pad_to_block(input):
    blocksize = 16
    assert len(input) <= 16
    diff = len(input) % blocksize
    # Pad the input to be a full block
    if diff != 0:
        input += "\x00" * (blocksize - diff)
    output = []
    return bytearray(input)

def bytearray_to_hexstring(input):
    return "".join("%02x" % b for b in input)

def xor(a, b):
    output = bytearray()
    assert(len(a) == len(b))
    for i in range(0, len(a), 1):
        output.append(a[i] ^ b[i])
    return output

def block_to_bit_string(block):
    output = ""
    assert(len(block) == 16)
    for byte in block:
        # bin returns 0bXXX, we need to truncate 0b
        bits = bin(byte)[2:]
        output += "0" * (8 - len(bits)) + bits
    return output

def block_to_coefficient(block, galois_field):
    output = 0
    for exponent, bit in enumerate(block_to_bit_string(block)):
        if int(bit) == 1:
            output += pow(galois_field, exponent)
    return output

def polynomial_to_block(polynomial):
    value = 0
    output = bytearray()
    for position, bit in enumerate(polynomial._vector_()):
        value |= int(bit) << (127 - position)
    # Adapted from https://stackoverflow.com/a/14613164
    for position in reversed(range(16)):
        output.append((value & (0xff << position*8)) >> position*8)
    return output

### Initialization
if len(sys.argv) < 4 or sys.argv[1] == "--help":
    print ("Usage:", sys.argv[0], "<plaintext_with_tag1.bin> <plaintext_with_tag2.bin> <target_plaintext_without_tag.bin>")
    exit(1)

length1, plaintext1, auth_tag1 = read_input(sys.argv[1])
print "Plaintext 1:", bytearray_to_hexstring(plaintext1)
print "Auth-tag 1:", bytearray_to_hexstring(auth_tag1)

length2, plaintext2, auth_tag2 = read_input(sys.argv[2])
print "Plaintext 2:", bytearray_to_hexstring(plaintext2)
print "Auth-tag 2:", bytearray_to_hexstring(auth_tag2)

target_length, target_plaintext = read_forgery(sys.argv[3])
print "Forgery target:", bytearray_to_hexstring(target_plaintext)

assert length1 == length2 == target_length

# We operate in GF(2^128), thus XOR of coefficients == Addittion of coefficients
# https://crypto.stackexchange.com/q/59156
# This reduces complexity afterwards and improves readability a lot.
# The coefficients are generated in `blocks_to_coefficients`
auth_tag_stew = xor(auth_tag1, auth_tag2)
plaintext_stew = xor(plaintext1, plaintext2)

F, a = GF(pow(2, 128), name="a").objgen()
R, X = PolynomialRing(F, name="X").objgen()

Pm = block_to_coefficient(plaintext_stew, a)
P1 = block_to_coefficient(plaintext1, a)
P3 = block_to_coefficient(target_plaintext, a)
Sm = block_to_coefficient(auth_tag_stew, a)
S1 = block_to_coefficient(auth_tag1, a)

f_H = P1 * X + S1 + P3 * X

# g'1+2 == g'1 ^ g'2. Because we XORed the plaintexts, this is a condensed form.
gm_X = Pm * X + Sm

## There might be multiple possible tags
count = 1
for root, _ in gm_X.roots():
    target_auth_tag_candidate = f_H(root)
    print "Candidate Tag", count, bytearray_to_hexstring(polynomial_to_block(target_auth_tag_candidate))
    with open(sys.argv[3] + "_candidate_"+str(count)+".bin",'w') as f:
        f.write(target_plaintext[0:target_length])
        f.write(polynomial_to_block(target_auth_tag_candidate))
    count += 1
